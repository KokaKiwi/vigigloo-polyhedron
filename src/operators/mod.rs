use std::sync::Arc;

use kube::runtime::controller::Action;

#[macro_use]
mod util;

pub type Error = kube::runtime::finalizer::Error<crate::Error>;

operators!(
    #[derive(clap::ValueEnum)]
    Operator {
        /// Simple echo service
        Echo => echo,
    }
);

fn on_error<K, C>(_res: Arc<K>, err: &Error, _ctx: Arc<C>) -> Action {
    use std::time::Duration;

    tracing::error!("Errored during reconciliation: {err}");
    Action::requeue(Duration::from_secs(10))
}
