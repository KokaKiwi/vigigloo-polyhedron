use kube::Resource;

#[easy_ext::ext(ResourceExt)]
pub impl<T> T
where
    Self: Resource,
{
    fn namespace_or_default<'a, 'b: 'a>(&'a self, client: &'b kube::Client) -> &str {
        self.meta()
            .namespace
            .as_deref()
            .unwrap_or(client.default_namespace())
    }
}
